from django.shortcuts import render, redirect

# Create your views here.
def index(request):
    user = request.user
    if user.is_authenticated:
        return redirect('/dashboard')
    return render(request, 'index.html')