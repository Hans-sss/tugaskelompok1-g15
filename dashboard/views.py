from django.shortcuts import render
from announcement.models import AnnouncementModel
from course.models import Enrollment
from account.decorators import allowed_user, unauthenticated_user
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='/account/login/')
def dashView(request):
    boolAnn = False
    Ann = AnnouncementModel.objects.all()
    if len(Ann) > 0:
        boolAnn = True;
    User = request.user
    Role = User.groups.values_list('name',flat = True)
    Roles = list(Role)
    boolCourse = False;
    list_course = Enrollment.objects.filter(member=User)
    if len(list_course) > 0:
        boolCourse = True;
    context = {
        'boolAnn':boolAnn,
        'Anns':Ann,
        'Roles':Roles,
        'User':User,
        'boolCourse':boolCourse,
        'Enrolled':list_course,
    }
    return render(request,'dashboard.html', context)