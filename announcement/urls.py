from django.urls import path
from . import views

app_name = 'announcement'

urlpatterns = [
    path('<int:idAn>/view',views.AnnView, name='viewAn'),
    path('add', views.AnnAdd, name='addAn'),
    path('<int:idAn>/edit', views.AnnEdit, name='editAn'),
    path('<int:idAn>/delete', views.AnnDelete, name='deleteAn'),
]