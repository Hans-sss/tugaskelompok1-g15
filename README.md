# TugasKelompok1-G15

Status Pipeline : 
[![pipeline status](https://gitlab.com/Hans-sss/tugaskelompok1-g15/badges/master/pipeline.svg)](https://gitlab.com/Hans-sss/tugaskelompok1-g15/-/commits/master)

Status Coverage :
[![coverage report](https://gitlab.com/Hans-sss/tugaskelompok1-g15/badges/master/coverage.svg)](https://gitlab.com/Hans-sss/tugaskelompok1-g15/-/commits/master)

Anggota Kelompok:
- Muhamad Akbar Syahputra Adha - 1906399530 (Announcement)
- Hernowo Ari Sutanto - 1906400274 (Account)
- Rayhan Maulana Akbar - 1906399524 (Course bagian Course)
- Samuel Sharon Sidabutar - 1906400204 (Course bagian Week)
- Muhammad Naufal Khairullah - 1906400356 (To-do List)


Link herokuapp:
https://tugaskelompok1-g15.herokuapp.com/

Aplikasi:
 
SEKELE adalah sebuah platform yang berfokus pada layanan dibidang pendidikan. SEKELE diharapkan dapat menunjang kegiatan belajar-mengajar selama masa pandemi COVID-19. Platform ini dibuat untuk memudahkan seorang Pengajar/Dosen berinteraksi dengan Mahasiswa-nya atau Mahasiswa dengan Mahasiswa lainnya pada saat Pembelajaran Jarak Jauh (PJJ) dengan berbagai fitur-fitur yang disediakan didalam platform tersebut.

Fitur:
- Buat akun pengguna
- Sistem Log-in 
- Laman profil akun
- Sistem Enrol/Un-enrol 
- Unggah Materi 
- Announcement Dosen/Pengajar
- Pengajar/Dosen Set-up Minggu pembelajaran


Akun Pengajar:
- Nomor Induk : 00000001
- Password : admintkg15

Akun Superuser:
- Nomor Induk: 87654321
- Password : admintkg15

Akun Student:
- Nomor Induk: 00000002
- Password : admintkg15