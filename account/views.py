from course.models import Enrollment
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login as auth_login, authenticate, logout as auth_logout
from account.decorators import allowed_user, unauthenticated_user
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from .forms import FormRegistrasi, AccountAuthenticationForm
# Create your views here.

def register_view(request, *args, **kwargs):
    user  = request.user
    if user.is_authenticated:
        return redirect('/dashboard')
    context = {}

    if request.POST:
        form = FormRegistrasi(request.POST)
        if form.is_valid():
            user = form.save()
            nomorinduk = form.cleaned_data.get('nomorinduk')
            raw_password = form.cleaned_data.get('password1')
            account = authenticate(nomorinduk=nomorinduk, password=raw_password)
            group = Group.objects.get(name='student')
            user.groups.add(group)
            auth_login(request, account)
            return HttpResponseRedirect('/account/login')
        else:
            context['registration_form'] = form

    return render(request, 'account/register.html', context)

def login_view(request, *args, **kwargs):
    context = {}
    user = request.user
    if user.is_authenticated:
        return redirect('/dashboard')

    if request.POST:
        form = AccountAuthenticationForm(request.POST)
        if form.is_valid():
            nomorinduk = request.POST['nomorinduk']
            raw_password = request.POST['password']
            user = authenticate(nomorinduk=nomorinduk, password=raw_password)
            group = Group.objects.get(name='student')
            user.groups.add(group)
            if user:
                auth_login(request, user)
                return redirect("/dashboard")
        else:
            context['login_form'] = form

    return render(request, "account/login.html", context)

@login_required(login_url='/account/login/')
@allowed_user(allowed_roles=['lecturer', 'student'])
def logout_view(request):
    auth_logout(request)
    return redirect('/')


@login_required(login_url='/account/login/')
def profile_view(request):
    User = request.user
    list_enroll = Enrollment.objects.filter(member=User)
    context={
        'User'          : User,
        'list_enroll'   : list_enroll,
    }
    return render(request, "account/profile.html", context)
